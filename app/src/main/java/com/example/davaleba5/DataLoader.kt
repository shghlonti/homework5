package com.example.davaleba5

import android.util.Log.d
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://reqres.in/api/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(path: String, customCallback: CustomCallback){
        val call: Call<String> = service.getRequest(path)
        call.enqueue(callBack(customCallback))
    }
    fun postRequest(path: String,parameters:MutableMap<String,String>,customCallback : CustomCallback){
        val call= service.postRequest(path,parameters)
        call.enqueue(callBack(customCallback))
    }
    private fun callBack(customCallback: CustomCallback)=  object :Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("getRequest","${t.message}")
            customCallback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("getRequest","${response.body()}")
            customCallback.onSucces(response.body().toString())
        }

    }
}

interface ApiRetrofit {
    @GET( "{path}")
    fun getRequest(@Path("path") path: String): Call<String>
    @POST( "{path}")
    fun postRequest(@Path("path") path: String,parameter : Map<String,String>): Call<String>
}

