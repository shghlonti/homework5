package com.example.davaleba5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import com.google.gson.Gson

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getUsers()
    }

    private fun getUsers(){
        DataLoader.getRequest("users",object :CustomCallback{
            override fun onSucces(result: String) {
                val model = Gson().fromJson(result,UserModel::class.java)
                d("userCount","${model.data.size}")
                d("avatar", model.data[0].avatar)
            }
        })
    }

}
