package com.example.davaleba5

import com.google.gson.annotations.SerializedName

class UserModel {
    var page = 0
    @SerializedName("per_page")
    var per_Page = 0
    var total = 0
    @SerializedName("total_page")
    var total_Pages = 0
    lateinit var data:MutableList<Data>

    inner class Data{
        var id = 0
        var email = ""
        var name = ""
        var surname = ""
        var avatar = ""
    }
}